
<div class="col-md-4 item-post">
    <a href="<?php the_permalink(); ?>">
        <div class="thumb">


        <?php if(has_post_thumbnail(get_the_ID())){
            ?>
            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt="">
            <?php
        } else{
            ?>
            <img src="<?php echo get_template_directory_uri() ?>/img/no-img.png" alt="">
            <?php
        }
        ?>
        </div>
        <span class="title"><?php the_title('');?></span>
        <div class="event-info">
            <span class="date"><i class="fa fa-calendar"></i><?php
                $date = get_post_meta(get_the_ID(), 'wpcf-date', true);
                $date = date_i18n('F j, Y', $date);
                echo $date;
                ?></span>
            <span class="city"><i class="fa fa-map-marker"></i><?php echo get_post_meta(get_the_ID(), 'wpcf-city', true);?></span>
        </div>
    </a>
</div>