<?php
/*
 * Template Name: Press-releases archive
 */
get_header();
the_post();
?>

    <div class="container">

        <div class="row">


        <span class="page-title">
            <?php the_title('');?>
        </span>
            <?php
            $content = get_the_content();
            if(!empty($content)){
                ?>
                <div class="content">
                    <?php echo  $content;?>
                </div>
                <?php
            }
            ?>


            <div class="section-header">
                <span class="section-header-text"><b>Пресс</b> релизы</span>

                <div class="pull-right">


                </div>
            </div>

            <div class="posts-container">

                <?php

                $today = strtotime('today');
                $query = new WP_Query(array(
                    'post_type' => 'press-releases',
                    'post_per_page' => 9,

                ));
                while ($query->have_posts()){
                    $query->the_post();
                    get_template_part('template-parts/content','release');
                }
                ?>
            </div>
            <a class="yellow-button col-md-12 load-more"   href="#">Загрузить больше релизов</a>
        </div>
    </div>

<?php
get_footer();