<?php
get_header();
the_post();?>
    <section class="container-fluid">
        <div class="row">
            <div class="container">
                <h2><?php the_title('');?></h2>
                <div class="page-content">
                    <?php the_content();?>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();