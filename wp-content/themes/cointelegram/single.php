<?php
get_header();
the_post();
setPostViews(get_the_ID());
?>

<div class="container">
    <div class="row">
        <span class="single-title"><?php the_title('');?></span>
        <div class="section-header">
            <?php
                $author = get_post_meta(get_the_ID(), 'wpcf-author', true);
                if(!empty($author)){
                   ?>
                    <span class="section-header-text"><b>Автор</b> <?php echo $author;?></span>
                    <?php
                }else if(get_the_category()){
                    $category = get_the_category();
                    ?>
                    <span class="section-header-text"><?php echo $category[0]->name;?></span>
                    <?php

                }else{
                    ?>
                    <span class="section-header-text">Событие</span>
                    <?php

                }
            ?>


            <div class="pull-right">
                <?php
                $date = get_post_meta(get_the_ID(), 'wpcf-date', true);
                if(!empty($date)){
                    ?>
                    <span class="date">
                        <?php
                        $date = date_i18n('F j, Y', $date);
                        echo $date;
                        ?>
                    </span>
                <?php
                }else{
                    ?>
                    <span class="date">
                        <?php
                        the_date('F j, Y');
                        ?>
                    </span>
                <?php
                }
                ?>

                <span class="views">
                    <i class="fa fa-eye"></i>
                    <?php
                        echo getPostViews(get_the_ID());
                    ?>
                </span>
            </div>
        </div>

        <div class="single-post-content">
            <?php
            the_content();
            ?>
            <?php
//            $tags = get_tags(array(
//                'hide_empty' => false
//            ));
            $tags = wp_get_post_tags(get_the_ID());
            if(!empty($tags)){

            echo '<ul class="tag-list">';
            foreach ($tags as $tag) {
                echo '<li><a href="'.  get_tag_link( $tag->term_id ) .'">' . $tag->name . '</a></li>';
            }
            echo '</ul>';
            }

            ?>
        </div>

        <?php
        if ( comments_open() || get_comments_number() ) :

            comments_template();
        endif;
        ?>
    </div>
</div>

<?php
get_footer();