<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php wp_head();?>
</head>
<body>
<header>
    <div class="widget-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 logo">
                    <?php the_custom_logo();?>
                    <span class="site-name"><span style="color: #fcd10e;">coin</span>telegram</span>
                </div>
                <div class="col-md-3 col-md-offset-6">
                    <a href="#" class="yellow-button pull-right">Submit your post</a>
                </div>
            </div>
        </div>
    </div>


    <div class="navigation">
        <div class="container">
            <div class="row">
                <div class="pull-left">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'top',
                        'menu_id'        => 'main-menu',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'bs-example-navbar-collapse-1',
                        'menu_class'        => 'nav navbar-nav',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker()
                    ))
                    ?>
                </div>
                <div class="pull-right">
                    <?php echo get_search_form();?>
                </div>
            </div>

        </div>
    </div>


</header>