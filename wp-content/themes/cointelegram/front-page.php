<?php
get_header();?>
    <section class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row">
                    <section class="trending-posts">
                        <div class="col-md-8 main-post">
                            <?php
                            $query = new WP_Query(array(
                                'post_type' => 'post',
                                'posts_per_page' => 1,
                                'meta_key'=> 'wpcf-ismainpost',
                                'meta_query' => array(
                                    'key' => 'wpcf-ismainpost',
                                    'value' => true,
                                    'compare' => 'LIKE',
                                    'type'  => 'BOOLEAN'
                                )

                            ));
                            if($query->have_posts()){
                                while($query->have_posts()){
                                    $query->the_post();
                                    ?>
                                    <div class="thumbnail">
                                        <?php if(has_post_thumbnail(get_the_ID())){
                                            ?>
                                            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt="">
                                            <?php
                                        } else{
                                            ?>
                                            <img src="<?php echo get_template_directory_uri() ?>/img/no-img.png" alt="">
                                            <?php
                                        }
                                        ?>
                                        <div class="overlay"></div>
                                        <div class="main-post-info">
                                            <div class="title">
                                                <?php the_title('');?>
                                            </div>
                                            <div class="meta">
                                                <span class="date"><?php  echo get_the_date('F j, Y');?></span>
                                                <span class="views"><i class="fa fa-eye"></i> <?php echo getPostViews(get_the_ID());?></span>
                                            </div>
                                            <div class="read-more">
                                                <a href="<?php the_permalink(); ?>">Читать больше</a>
                                            </div>
                                        </div>
                                        <div class="category">
                                            <span>
                                                <?php $category = get_the_category();
                                                echo $category[0]->name;
                                                ?>
                                            </span>
                                        </div>
                                    </div>
                                    <?
                                }
                            }
                            ?>

                        </div>
                        <div class="col-md-4 recent-list">
                            <span class="header"><b>Trending</b> posts</span>

                            <ul>
                                <?php
                                $query = new WP_Query( array(
                                    'post_type' => 'post',
                                    'posts_per_page'=>4,
                                    'meta_key' => 'post_views_count',
                                    'orderby' => 'meta_value_num',
                                    'order' => 'DESC'

                                ) );
                                while ( $query->have_posts() ) {
                                    $query->the_post();
                                    ?>
                                        <li>
                                            <a href="<?php the_permalink(); ?>">
                                                <span class="title"><?php the_title();?></span>
                                                <div class="meta">
                                                    <span class="pull-left date"><?php the_date('F j, Y');?></span>
                                                    <span class="pull-right views"><i class="fa fa-eye"></i> <?php echo getPostViews(get_the_ID());?></span>
                                                </div>
                                            </a>
                                        </li>
                                    <?php
                                }
                                    ?>

                            </ul>
                            <div class="more-posts">
                                <a href="#" >Больше постов</a>
                            </div>
                        </div>
                    </section>
                    <section class="press-relaeses-short">
                        <div class="section-header">
                            <span class="section-header-text"><b>Пресс</b> релизы</span>

                            <div class="pull-right">
                                <div class="carousel-controls pull-left" data-target="press-releases">
                                    <div class="left pull-left">
                                        <i class="fa fa-angle-left"></i>
                                    </div>
                                    <div class="right pull-right">
                                        <i class="fa fa-angle-right"></i>
                                    </div>
                                </div>
                                <div class="filter-toggle pull-left" data-target="">
                                    <i class="fa fa-bars"></i>
                                </div>
                            </div>
                        </div>
                        <div class="owl-carousel" id="press-releases">
                            <?php
                            $query = new WP_Query( array(
                                'post_type' => 'press-releases',
                                'posts_per_page'=>6,
                            ) );
                            while ( $query->have_posts() ) {
                                $query->the_post();
                                ?>
                                <div class="press-release-item">
                                    <a href="<?php the_permalink();?>">

                                        <?php if(has_post_thumbnail(get_the_ID())){
                                            ?>
                                            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt="">
                                            <?php
                                        } else{
                                            ?>
                                            <img src="<?php echo get_template_directory_uri() ?>/img/no-img.png" alt="">
                                            <?php
                                        }
                                        ?>
                                        <div class="press-release-info">
                                            <span class="press-release-title"><?php the_title('');?></span>
                                            <div>
                                                <span class="date"><?php echo get_the_date('F j, Y');?></span>
                                                <span class="views"><i class="fa fa-eye"></i> <?php echo getPostViews(get_the_ID());?></span>
                                            </div>

                                        </div>
                                    </a>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="press-releases-links row">
                            <div class="col-md-6">
                                <a href="#" class="yellow-button">Больше релизов</a>
                            </div>
                            <div class="col-md-6">
                                <a href="#" class="yellow-button">Предложить релиз</a>
                            </div>
                        </div>
                    </section>
                    <section class="latest-news">
                        <div class="container">
                            <div class="row">
                                <div class="section-header">
                                    <span class="section-header-text"><b>Последние</b> новости</span>

                                    <div class="pull-right">
                                        <div class="categories-menu pull-left">

                                            <?php
                                            $categories = get_categories();
                                            ?>
                                            <ul id="category-menu">
                                                <li id="all"><a class="ajax active" href="#" onclick="cat_ajax_get(event, 'all')">All</a></li>
                                                <?php foreach ( $categories as $cat ) { ?>
                                                    <li id="<?php echo $cat->term_id; ?>"><a class="<?php echo $cat->slug; ?> ajax" onclick="cat_ajax_get(event, '<?php echo $cat->term_id; ?>');" href="#"><?php echo $cat->name; ?></a></li>

                                                <?php } ?>
                                            </ul>
                                        </div>
                                        <div class="filter-toggle pull-left" data-target="">
                                            <i class="fa fa-bars"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="latest-news-list" id="latest-news">
                                    <?php
                                    $query = new WP_Query(array(
                                        'post_type' => 'post',
                                        'posts_per_page' => 9
                                    ));
                                    while($query->have_posts()){
                                        $query->the_post();
                                        ?>
                                        <div class="latest-news-item col-md-4">
                                            <a href="<?php the_permalink();?>">
                                                <div class="thumbnail">


                                                <?php if(has_post_thumbnail(get_the_ID())){
                                                    ?>
                                                    <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt="">
                                                    <?php
                                                } else{
                                                    ?>
                                                    <img src="<?php echo get_template_directory_uri() ?>/img/no-img.png" alt="">
                                                    <?php
                                                }
                                                ?>
                                                    <span class="news-cat">
                                                        <?php $category = get_the_category();
                                                            echo $category[0]->name;
                                                        ?>
                                                    </span>
                                                </div>
                                                <span class="news-title"><?php the_title('');?></span>
                                                <div class="news-info">
                                                    <span class="date"><?php echo get_the_date('F j, Y');?></span>
                                                    <span class="author"><b><?php echo get_post_meta(get_the_ID(), 'wpcf-author', true);?></b></span>
                                                </div>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                </div>
                                <a class="yellow-button col-md-12"  onclick="loadMoreNews(event);" href="#">Загрузить больше новостей</a>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();