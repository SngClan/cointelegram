<?php
get_header();
?>
<div class="container">

    <div class="row">

        <div class="section-header">
            <span class="section-header-text"><?php single_tag_title();?></span>

            <div class="pull-right">


            </div>
        </div>

        <div class="posts-container" id="">
            <?php
            $tag = get_queried_object();
            $query = new WP_Query(array(
                'post_type'     => array('post', 'event', 'press-releases'),
                'tag'           => $tag->slug,
                'posts_per_page' => 9,
            ));

            while($query->have_posts()){
                $query->the_post();


                $type = get_post_type();

                    switch ($type){
                        case 'post':{
                            ?>
                            <div class="item-post col-md-4">
                                <a href="<?php the_permalink();?>">
                                    <div class="thumbnail">

                                        <?php if(has_post_thumbnail(get_the_ID())){
                                            ?>
                                            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt="">
                                            <?php
                                        } else{
                                            ?>
                                            <img src="<?php echo get_template_directory_uri() ?>/img/no-img.png" alt="">
                                            <?php
                                        }
                                        ?>

                                    </div>
                                    <span class="news-title"><?php the_title('');?></span>
                                    <div class="news-info">
                                        <span class="date"><?php echo get_the_date('F j, Y');?></span>
                                        <span class="author"><b><?php echo get_post_meta(get_the_ID(), 'wpcf-author', true);?></b></span>
                                    </div>
                                </a>
                            </div>
                            <?php
                            break;
                        }
                        case 'event':{
                            get_template_part('template-parts/content','event');
                            break;
                        }
                        case 'press-releases':{
                            get_template_part('template-parts/content','release');
                            break;
                        }
                    }
                ?>


                <?php

            }
            ?>
        </div>
        <a class="yellow-button col-md-12 load-more"   href="#">Загрузить больше</a>
    </div>
</div>

<?php
get_footer();