<div class="footer-widgets">
    <div class="container">
        <div class="row">
            <div class="col-md-1">
                <?php the_custom_logo();?>
            </div>
            <div class="col-md-5">
                <?php
                if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ){
                    dynamic_sidebar('about');
                }
                ?>

            </div>
            <div class="col-md-3">
                <?php dynamic_sidebar('contact');?>
            </div>
            <div class="col-md-3">
                <?php
                dynamic_sidebar('social-menu')
                ?>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">


            <div class="pull-left">
                <?php
                wp_nav_menu(array(
                        'theme_location' => 'bottom'
                ))
                ?>
            </div>
            <div class="pull-right">
                <?php
                dynamic_sidebar('desc');
                ?>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer();
?>
</body>
</html>
