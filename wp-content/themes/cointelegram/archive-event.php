<?php
/*
 * Template Name: Events archive
 */
get_header();
the_post();
?>

<div class="container">

    <div class="row">


        <span class="page-title">
            <?php the_title('');?>
        </span>
        <?php
        $content = get_the_content();
        if(!empty($content)){
            ?>
            <div class="content">
                <?php echo  $content;?>
            </div>
        <?php
        }
        ?>

        <div class="section-header">
            <span class="section-header-text"><b>В скором</b> времени</span>

            <div class="pull-right">
                <div class="carousel-controls pull-left" data-target="future-events">
                    <div class="left pull-left">
                        <i class="fa fa-angle-left"></i>
                    </div>
                    <div class="right pull-right">
                        <i class="fa fa-angle-right"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="posts-container future-events">
            <div class="owl-carousel" id="future-events">

            <?php
            $today = strtotime('today');
            $query = new WP_Query(array(
                'post_type' => 'event',
                'post_per_page' => 9,
                'meta_key' => 'wpcf-date',
                'meta_query' => array(
                    'key'=> 'wpcf-date',
                    'value' => strtotime('today'),
                    'compare' => '>=',
                    'type' => 'NUMERIC'
                )
            ));
            while ($query->have_posts()){
                $query->the_post();
                get_template_part('template-parts/content','eventCar');
            }
            ?>
            </div>
        </div>

        <div class="section-header">
            <span class="section-header-text"><b>Отчеты прошедших</b> событий</span>

            <div class="pull-right">


            </div>
        </div>

        <div class="posts-container" id="old-events">

            <?php

            $today = strtotime('today');
            $query = new WP_Query(array(
               'post_type' => 'event',
               'post_per_page' => 9,
                'meta_key' => 'wpcf-date',
                'meta_query' => array(
                    'key'=> 'wpcf-date',
                    'value' => $today,
                    'compare' => '<=',
                    'type' => 'NUMERIC'
                )
            ));
            while ($query->have_posts()){
                $query->the_post();
                get_template_part('template-parts/content','event');
            }
            ?>
        </div>
        <a class="yellow-button col-md-12 load-more"   onclick="loadMoreEvents(event);" href="#">Загрузить больше событий</a>
    </div>
</div>

<?php
get_footer();