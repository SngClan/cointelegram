$(document).ready(function () {
     $('.search-form .search-submit').click(function (e) {

        if(!$('.search-form .search-field').is(':visible')){
            e.preventDefault();
            $('.search-form .search-field').toggleClass('opened');
            $('.search-form .search-field').animate({width:'toggle'},500);
        }else if($('.search-form .search-field').is(':visible')&&!$('.search-form .search-field').val()){
            e.preventDefault();
            $('.search-form .search-field').animate({width:'toggle'},500);
        }
    });
    $('#press-releases').owlCarousel({
        items: 3,
        loop: true
    });
    $('.carousel-controls .left').click(function (e) {
        $('#'+$(this).parent().attr('data-target')).trigger('prev.owl.carousel', [500]);
    });
    $('.carousel-controls .right').click(function (e) {
        $('#'+$(this).parent().attr('data-target')).trigger('next.owl.carousel', [500]);
    });
    $('#future-events').owlCarousel({
        loop: true,
        margin: 30,
        items: 3
    })
    $('#comments-open').click(function (e) {
        $('#comments-wrapp').slideToggle();
    })
});
var page = 0;

function cat_ajax_get(e, catID) {
    e.preventDefault();

    page=0;
    $("a.ajax").removeClass("active");
    $(e.target).addClass('active');
    var str = '&cat=' + catID +'&page='+page+'&action=prefix_load_cat_posts';

    $.ajax({
        type: "POST",
        dataType: "html",
        url: ajax_posts.ajaxurl,
        data: str,
        success: function(data){
            var $dat = $(data);
            $("#latest-news").html(data);
        },
        error : function(jqXHR, textStatus, errorThrown) {
            $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
        }

    });
    return false;
}

function loadMoreNews(e) {
    e.preventDefault();
    $(e.target).html("Загрузка...");
    page++;
    var cat = $('.categories-menu ul li .active').parent().attr('id');
    var str = '&cat=' + cat +'&page='+page+'&action=prefix_load_cat_posts';
    // console.log(str);
    $.ajax({
        type: "POST",
        dataType: "html",
        url: ajax_posts.ajaxurl,
        data: str,
        success: function(data){
            var $dat = $(data);
            $("#latest-news").append(data);
            $(e.target).html("Загрузить больше новостей");
        },
        error : function(jqXHR, textStatus, errorThrown) {
            $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
        }

    });
    return false;
}

function loadMorePosts(e) {
    e.preventDefault();
    $(e.target).html("Загрузка...");
    page++;
    var str = '&page='+page+'&action=load_more_posts';
    // console.log(str);
    $.ajax({
        type: "POST",
        dataType: "html",
        url: ajax_posts.ajaxurl,
        data: str,
        success: function(data){
            var $dat = $(data);
            $("#latest-news").append(data);
            $(e.target).html("Загрузить больше новостей");
        },
        error : function(jqXHR, textStatus, errorThrown) {
            $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
        }

    });
    return false;
}
function loadMoreEvents(e) {
    e.preventDefault();
    $(e.target).html("Загрузка...");
    page++;
    var str = '&page='+page+'&action=load_more_events';
    // console.log(str);
    $.ajax({
        type: "POST",
        dataType: "html",
        url: ajax_posts.ajaxurl,
        data: str,
        success: function(data){
            var $dat = $(data);
            $("#old-events").append(data);
            $(e.target).html("Загрузить больше событий");
        },
        error : function(jqXHR, textStatus, errorThrown) {
            $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
        }

    });
    return false;
}