<?php
add_action( 'wp_enqueue_scripts', 'theme_scripts' );
function theme_scripts() {
    global $wp_query;
    wp_enqueue_style( 'normalize.css', get_template_directory_uri() . '/css/normalize.css' );
    wp_enqueue_style( 'bootstrap.min.css', get_template_directory_uri() . '/css/bootstrap.min.css' );
    wp_enqueue_style( 'owl.carousel.min.css', get_template_directory_uri() . '/css/owl.carousel.min.css' );
//    wp_enqueue_style( '3dcar.css', get_template_directory_uri() . '/css/3dcar.css' );
    wp_enqueue_style( 'font-awesome.min.css', get_template_directory_uri() . '/css/font-awesome.min.css' );
    wp_enqueue_style( 'fonts', 'https://fonts.googleapis.com/css?family=Inconsolata|Poppins:400,700,800' , false);
    wp_enqueue_style( 'style.css', get_template_directory_uri() . '/css/style.css'  );
    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-3.2.1.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'owl.carousel.min.js', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array('jquery'), '1.0.0', true );
    wp_localize_script( 'script', 'ajax_posts', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'noposts' => __('No older posts found', 'test'),
        'nonce' => wp_create_nonce('ajax-nonce')
    ));


}

require_once get_template_directory() . '/wp-bootstrap-navwalker.php';

function theme_setup() {

    add_theme_support( 'post-thumbnails' );
    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );



    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'top'    => __( 'Top Menu', 'cointelegram' ),
        'bottom'    => __( 'Bottom Menu', 'cointelegram' ),
        'social' => __( 'Social Links Menu', 'cointelegram' )
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );



    // Add theme support for Custom Logo.
    add_theme_support( 'custom-logo', array(
        'width'       => 250,
        'height'      => 250,
        'flex-width'  => true,
    ) );

    add_theme_support( 'custom-header' );

}
add_action( 'after_setup_theme', 'theme_setup' );

function cointelegram_widgets_init() {

    register_sidebar( array(
        'name'          => 'Сайдбар в футере 1',
        'description'   => 'ширина 5',
        'id'            => 'about',
        'before_widget' => '<div class="footer-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<span class="widget-title">',
        'after_title'   => '</span>',
    ) );
    register_sidebar( array(
        'name'          => 'Сайдбар в футере 2',
        'description'   => 'ширина 3',
        'id'            => 'contact',
        'before_widget' => '<div class="footer-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<span class="widget-title">',
        'after_title'   => '</span>',
    ) );
    register_sidebar( array(
        'name'          => 'Сайдбар в футере 3',
        'description'   => 'ширина 3',
        'id'            => 'social-menu',
        'before_widget' => '<div class="footer-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<span class="widget-title">',
        'after_title'   => '</span>',
    ) );
    register_sidebar( array(
        'name'          => 'Сайдбар в футере',
        'description'   => 'внизу справа',
        'id'            => 'desc',
        'before_widget' => '<div class="footer-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<span class="widget-title">',
        'after_title'   => '</span>',
    ) );

}
add_action( 'widgets_init', 'cointelegram_widgets_init' );

function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10);

add_action( 'wp_ajax_nopriv_prefix_load_cat_posts', 'prefix_load_cat_posts' );
add_action( 'wp_ajax_prefix_load_cat_posts', 'prefix_load_cat_posts' );
function prefix_load_cat_posts () {
    $cat_id = $_POST[ 'cat' ];
    $page = $_POST[ 'page' ]+1;
    if($cat_id=='all'){
        $args = array(
            'posts_per_page' => 3,
            'paged' => $page
        );
    } else{
        $args = array (
            'cat' => $cat_id,
            'posts_per_page' => 3,
            'paged' => $page

        );
    }


    $posts = get_posts( $args );

    ob_start ();

    foreach ( $posts as $post ) {
        setup_postdata( $post ); ?>

        <div class="latest-news-item col-md-4">
            <a href="<?php echo get_the_permalink($post->ID);?>">
                <div class="thumbnail">


                    <?php if(has_post_thumbnail($post->ID)){
                        ?>
                        <img src="<?php echo get_the_post_thumbnail_url($post->ID) ?>" alt="">
                        <?php
                    } else{
                        ?>
                        <img src="<?php echo get_template_directory_uri() ?>/img/no-img.png" alt="">
                        <?php
                    }
                    ?>
                    <span class="news-cat">
                                                        <?php $category = get_the_category($post->ID);
                                                        echo $category[0]->name;
                                                        ?>
                                                    </span>
                </div>
                <span class="news-title"><?php echo get_the_title($post);?></span>
                <div class="news-info">
                    <span class="date"><?php echo get_the_date( 'F j, Y', $post->ID);?></span>
                    <span class="author"><b><?php echo get_post_meta($post->ID, 'wpcf-author', true);?></b></span>
                </div>
            </a>
        </div>

    <?php } wp_reset_postdata();

    $response = ob_get_contents();
    ob_end_clean();

    echo $response;
    die(1);
}

add_filter( 'storm_social_icons_networks', 'storm_social_icons_networks');
function storm_social_icons_networks( $networks ) {

    $extra_icons = array (
        'feed' => array(                  // Enable this icon for any URL containing this text
            'name' => 'RSS',               // Default menu item label
            'class' => 'rss',              // Custom class
            'icon' => 'icon-rss',          // FontAwesome class
            'icon-sign' => 'icon-rss-sign' // May not be available. Check FontAwesome.
        ),
        'youtube' => array(                  // Enable this icon for any URL containing this text
            'name' => 'Youtube',               // Default menu item label
            'class' => 'youtube',              // Custom class
            'icon' => 'icon-youtube-play',          // FontAwesome class
        ),
//        'telegram' => array(                  // Enable this icon for any URL containing this text
//            'name' => 'Telegram',               // Default menu item label
//            'class' => 'telegram',              // Custom class
//            'icon' => 'icon-telegram',          // FontAwesome class
//        ),
    );

    $extra_icons = array_merge( $networks, $extra_icons );
    return $extra_icons;

}

function disable_comment_url($fields) {
    unset($fields['url']);
    return $fields;
}
add_filter('comment_form_default_fields','disable_comment_url');
function move_comment_field_to_bottom( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}

add_filter( 'comment_form_fields', 'move_comment_field_to_bottom' );


function custom_comment($comment, $args, $depth){
    $GLOBALS['comment'] = $comment; ?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
    <div id="comment-<?php comment_ID(); ?>">
        <div class="comment-head">
            <div class="comment-author vcard pull-left">
                <span class="fn"><?php echo get_comment_author() ?></span>
                <span class="time"> &middot; <?php echo get_comment_date('F j, Y');?></span>
            </div>
            <div class="reply pull-right">
                <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </div>
        </div>

        <?php if ($comment->comment_approved == '0') : ?>
            <em>Ваш комментарий ожидает проверки.</em>
            <br />
        <?php endif; ?>

        <div class="comment-meta commentmetadata">
            <?php edit_comment_link('(Редактировать)', '  ', '') ?>
        </div>

        <?php comment_text() ?>


    </div>
    <?php
}

add_action( 'wp_ajax_nopriv_prefix_load_cat_posts', 'load_more_posts' );
add_action( 'wp_ajax_prefix_load_cat_posts', 'load_more_posts' );
function load_more_posts () {
    $page = $_POST[ 'page' ]+4;

    $args = array (
        'post_type' => 'post',
        'posts_per_page' => 3,
        'paged' => $page

    );



    $posts = get_posts( $args );

    ob_start ();

    foreach ( $posts as $post ) {
        setup_postdata( $post ); ?>

        <div class="latest-news-item col-md-4">
            <a href="<?php echo get_the_permalink($post->ID);?>">
                <div class="thumbnail">


                    <?php if(has_post_thumbnail($post->ID)){
                        ?>
                        <img src="<?php echo get_the_post_thumbnail_url($post->ID) ?>" alt="">
                        <?php
                    } else{
                        ?>
                        <img src="<?php echo get_template_directory_uri() ?>/img/no-img.png" alt="">
                        <?php
                    }
                    ?>
                    <span class="news-cat">
                                                        <?php $category = get_the_category($post->ID);
                                                        echo $category[0]->name;
                                                        ?>
                                                    </span>
                </div>
                <span class="news-title"><?php echo get_the_title($post);?></span>
                <div class="news-info">
                    <span class="date"><?php echo get_the_date( 'F j, Y', $post->ID);?></span>
                    <span class="author"><b><?php echo get_post_meta($post->ID, 'wpcf-author', true);?></b></span>
                </div>
            </a>
        </div>

    <?php } wp_reset_postdata();

    $response = ob_get_contents();
    ob_end_clean();

    echo $response;
    die(1);
}


add_action( 'wp_ajax_nopriv_prefix_load_cat_posts', 'load_more_events' );
add_action( 'wp_ajax_prefix_load_cat_posts', 'load_more_events' );
function load_more_events () {
    $page = $_POST[ 'page' ]+4;

    $args = array (
        'post_type' => 'event',
        'posts_per_page' => 3,
        'paged' => $page

    );



    $posts = get_posts( $args );

    ob_start ();

    foreach ( $posts as $post ) {
        setup_postdata( $post );

        ?>
        <div class="col-md-4 item-post">
            <a href="<?php echo get_the_permalink($post->ID); ?>">
                <div class="thumb">


                    <?php if(has_post_thumbnail($post->ID)){
                        ?>
                        <img src="<?php echo get_the_post_thumbnail_url($post->ID) ?>" alt="">
                        <?php
                    } else{
                        ?>
                        <img src="<?php echo get_template_directory_uri() ?>/img/no-img.png" alt="">
                        <?php
                    }
                    ?>
                </div>
                <span class="title"><?php echo get_the_title($post->ID);?></span>
                <div class="event-info">
            <span class="date"><i class="fa fa-calendar"></i><?php
                $date = get_post_meta($post->ID, 'wpcf-date', true);
                $date = date_i18n('F j, Y', $date);
                echo $date;
                ?></span>
                    <span class="city"><i class="fa fa-map-marker"></i><?php echo get_post_meta($post->ID, 'wpcf-city', true);?></span>
                </div>
            </a>
        </div>
    <?php

    } wp_reset_postdata();

    $response = ob_get_contents();
    ob_end_clean();

    echo $response;
    die(1);
}
