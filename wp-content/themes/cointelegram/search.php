<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="container">

	<header class="page-header">
		<?php if ( have_posts() ) : ?>
			<h1 class="page-title"><?php printf( __( 'Результіта поиска для: %s', 'cointelegram' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
		<?php else : ?>
			<h1 class="page-title"><?php _e( 'Ничего не найдено', 'cointelegram' ); ?></h1>
		<?php endif; ?>
	</header><!-- .page-header -->

		<main  class="posts-container search-result">

		<?php
		if ( have_posts() ) {
			/* Start the Loop */
			while ( have_posts() ) {
                the_post();


                /**
                 * Run the loop for the search to output the results.
                 * If you want to overload this in a child theme then include a file
                 * called content-search.php and that will be used instead.
                 */
                $type = get_post_type();

                switch ($type) {
                    case 'post': {
                        ?>
                        <div class="item-post col-md-4">
                            <a href="<?php the_permalink(); ?>">
                                <div class="thumbnail">

                                    <?php if (has_post_thumbnail(get_the_ID())) {
                                        ?>
                                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt="">
                                        <?php
                                    } else {
                                        ?>
                                        <img src="<?php echo get_template_directory_uri() ?>/img/no-img.png" alt="">
                                        <?php
                                    }
                                    ?>

                                </div>
                                <span class="news-title"><?php the_title(''); ?></span>
                                <div class="news-info">
                                    <span class="date"><?php echo get_the_date('F j, Y'); ?></span>
                                    <span class="author"><b><?php echo get_post_meta(get_the_ID(), 'wpcf-author', true); ?></b></span>
                                </div>
                            </a>
                        </div>
                        <?php
                        break;
                    }
                    case 'event': {
                        get_template_part('template-parts/content', 'event');
                        break;
                    }
                    case 'press-releases': {
                        get_template_part('template-parts/content', 'release');
                        break;
                    }

                }; // End of the loop.

//			the_posts_pagination( array(
//				'prev_text' => cointelegram_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'cointelegram' ) . '</span>',
//				'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'cointelegram' ) . '</span>' . cointelegram_get_svg( array( 'icon' => 'arrow-right' ) ),
//				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'cointelegram' ) . ' </span>',
//			) );

            }
		} else { ?>

                <p><?php _e('Sorry, but nothing matched your search terms. Please try again with some different keywords.'); ?></p>
                <?php
//				get_search_form();

            };
		?>

		</main><!-- #main -->

<!--	--><?php //get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
