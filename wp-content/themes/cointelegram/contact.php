<?php
/*
 * Template Name: Contact page
 */
get_header();
the_post();
?>
<div class="container">
    <div class="row">
        <span class="page-title">
            <?php the_title();?>
        </span>
        <?php
        $content = get_the_content();
        if(!empty($content)){
            ?>
            <div class="content">
                <?php echo  $content;?>
            </div>
            <?php
        }
        ?>

        <div class="section-header">
            <span class="section-header-text"><b>The price</b> per release is <b>0.5 BTC</b></span>
        </div>
        <div class="posts-container contact-form">



            <?php
            if ( function_exists( 'ccf_output_form' ) ) {
                ccf_output_form( 105 );
            }
//            echo do_shortcode('[contact-form-7 id="157" title="Контактная форма 1"]');
            ?>

        </div>
    </div>
</div>

<?php
get_footer();