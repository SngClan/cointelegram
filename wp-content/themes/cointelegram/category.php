<?php

get_header();
the_post();
?>

    <div class="container">

        <div class="row">


        <span class="page-title">
            <?php single_cat_title( '', true ); ?>
        </span>
            <?php
            $content = category_description();
            if(!empty($content)){
                ?>
                <div class="content">
                    <?php echo  $content;?>
                </div>
                <?php
            }
            ?>


            <div class="section-header">
                <span class="section-header-text"><b>Последние</b> новости</span>

                <div class="pull-right">


                </div>
            </div>

            <div class="latest-news-list" id="latest-news">
                <?php
                $current_category = get_queried_object();
                $query = new WP_Query(array(
                    'post_type' => 'post',
                    'posts_per_page' => 9,
                    'cat' => $current_category->cat_ID
                ));
                while($query->have_posts()){
                    $query->the_post();
                    ?>
                    <div class="latest-news-item col-md-4">
                        <a href="<?php the_permalink();?>">
                            <div class="thumbnail">


                                <?php if(has_post_thumbnail(get_the_ID())){
                                    ?>
                                    <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt="">
                                    <?php
                                } else{
                                    ?>
                                    <img src="<?php echo get_template_directory_uri() ?>/img/no-img.png" alt="">
                                    <?php
                                }
                                ?>
                                <span class="news-cat">
                                                        <?php $category = get_the_category();
                                                        echo $category[0]->name;
                                                        ?>
                                                    </span>
                            </div>
                            <span class="news-title"><?php the_title('');?></span>
                            <div class="news-info">
                                <span class="date"><?php echo get_the_date('F j, Y');?></span>
                                <span class="author"><b><?php echo get_post_meta(get_the_ID(), 'wpcf-author', true);?></b></span>
                            </div>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>
            <a class="yellow-button col-md-12 load-more"   href="#">Загрузить больше новостей</a>
        </div>
    </div>

<?php
get_footer();