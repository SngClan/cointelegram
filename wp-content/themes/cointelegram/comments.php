<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">
    <span class="yellow-button load-more" id="comments-open">Комментарии</span>
    <div id="comments-wrapp">
    <div class="section-header">
        <span class="section-header-text">
            <?php
            // You can start editing here -- including this comment!
            if ( have_comments() ) : ?>
			<?php
            $comments_number = get_comments_number();
            ?>
            <?php echo __('Комментарии ');
            echo '('.number_format_i18n( $comments_number ).')';
            ?>
        </span>
    </div>

    <div class="row">
        <div class="col-md-7">
            <ol class="comment-list">
                <?php
                    wp_list_comments( array(
                        'avatar_size' => 0,
                        'style'       => 'li',
                        'short_ping'  => true,
                        'reply_text'  => __('Reply'),
                        'callback'    => 'custom_comment'
                    ) );
                ?>
            </ol>
        </div>
    <?php

	endif; // Check for have_comments().

	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

		<p class="no-comments"><?php _e( 'Comments are closed.', 'cointelegram' ); ?></p>
	<?php
	endif;
        ?>
        <div class="col-md-5">

        <?php
        comment_form( array(
            'title_reply_before' => '<span class="reply-title">',
            'title_reply_after'  => '</span>',
            'comment_notes_before' => '',
            'comment_notes_after' => '',
            'title_reply'          =>'Оставить комментарий',
            'title_reply_to'       => __( 'Leave a Reply to %s' ),
        ) );
            ?>
        </div>
    </div>
    </div>
</div><!-- #comments -->
