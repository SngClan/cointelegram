module.exports = function(grunt) {

    grunt.initConfig({
        less: {
            development: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    "css/style.css": "less/style.less" // destination file and source file
                }
            }
        },
        autoprefixer:{
            dist:{
                files:{
                    'css/style.css':'css/style.css'
                }
            }
        },
        watch: {
            styles: {
                files: ['less/**/*.less' ], // which files to watch
                tasks: ['less', 'autoprefixer'],
                options: {
                    nospawn: true
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.registerTask('go', ['watch']);
}

