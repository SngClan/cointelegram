<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'cointelegram');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'M78tq>Po7T}=fW=,xe2`{UAu02 z8cH2`yg3A >?_Qb:3T,`2[cYK%<JJV%zYOK7');
define('SECURE_AUTH_KEY',  '?U-~jF2_x29lvC4x[e_Bo07;1rP&B3V`wH7&a`@q^|Rm*Q`er2}q83E_-}[Gg<GR');
define('LOGGED_IN_KEY',    'P^-GFatcv14:#&1ZYtv5)2?P,lz)S.CLV!`}#B9hd4&1g1yRVI]w*4`g,~#]s$F(');
define('NONCE_KEY',        '}:DXHMnVqhVRUUT$z2d1;@gs@a%&<ylY>k]~.f0>tq5+BM%aPy>;?>M<r4yDPkb/');
define('AUTH_SALT',        'G1NvC?rm@E9i*c:w{O?Y?e+n56JX!`{>md~6LTL[%h8KyB#oe6[ 3hP9.h)Kl59C');
define('SECURE_AUTH_SALT', 'Z;ShVmBE($yQ=7Y<x..pt(?p7[KSK$PVqqm{PW8I`cRlj97]9QvXfM8?x6)f)9a]');
define('LOGGED_IN_SALT',   '=!~`FmDvbO83j]gt%O@PWip-M)*>Bw1kaD0LL`3Z-owL;NxG/FgE~4N~sN1tEQ&P');
define('NONCE_SALT',       '+,c[Sz5|ud}.(q_HJeZlh_/~TM.YyDu|Pd,BC5MZf+X+B!c6eaP*5t#bl`k-u6x/');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
